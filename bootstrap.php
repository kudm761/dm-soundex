<?php
require_once __DIR__ . '/vendor/autoload.php';

spl_autoload_register(function ($class) {
    $path = __DIR__ . '/src/' . $class . '.php';

    $prepared_path = str_replace('\\', '/', $path);
    $prepared_path = realpath($prepared_path);

    if (!file_exists($prepared_path)) {
        $prepared_path = __DIR__ . '/tests/' . $class . '.php';
    }

    if (file_exists($prepared_path)) {
        require_once $prepared_path;
    }
});