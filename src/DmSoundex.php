<?php
namespace DmSoundex;

class DmSoundex
{
    protected $consonants = ['бвгджзйклмнпрстфхцчшщъь'];

    protected $vulnerable_aliases = [
        'йо' => 'и',
        'ио' => 'и',
        'йе' => 'и',
        'ие' => 'и',
        'о' => 'а',
        'ы' => 'а',
        'я' => 'а',
        'е' => 'и',
        'ё' => 'и',
        'э' => 'и',
        'ю' => 'у',
        'тс' => 'ц',
        'дс' => 'ц'
    ];

    /**
     * @var array
     */
    protected $codes = [];

    public function __construct()
    {
        $codes_source = realpath(__DIR__ . '/../config/codes.php');

        if (empty($codes_source) || !file_exists($codes_source)) {
           throw new \Exception('Config file `codes` not found');
        }

        $this->codes = include $codes_source;
    }

    /**
     * @param string $string
     * @param bool $is_cyrillic
     * @return string
     */
    protected function getWordSound(string $string, bool $is_cyrillic = true) : string
    {
        $length = strlen($string);
        $output = '';
        $power = 6;
        $i = 0;

        $previous = -1;

        while ($i < $length) {
            $current = $last = &$this->codes[$string[$i]];

            for ($j = $k = 1; $k < $power + 1; $k++) {
                if (!isset($string[$i + $k]) || !isset($current[$string[$i + $k]])) {
                    break;
                }

                $current = &$current[$string[$i + $k]];

                if (isset($current[0])) {
                    $last = &$current;
                    $j = $k + 1;
                }
            }

            if ($i == 0) {
                $code = $last[0][0];
            } elseif (!isset($string[$i + $j]) || ($this->codes[$string[$i + $j]][0][0] <> 0)) {
                $code = $is_cyrillic ? (isset($last[1]) ? $last[1][2] : $last[0][2]) : $last[0][2];
            } else {
                $code = $is_cyrillic ? (isset($last[1]) ? $last[1][1] : $last[0][1]) : $last[0][1];
            }

            if (($code <> -1) && ($code <> $previous)) {
                $output .= $code;
            }

            $previous = $code;

            $i += $j;
        }

        return str_pad(substr($output, 0, $power), $power, '0');
    }

    /**
     * @param string $string
     * @return string
     */
    protected function replaceVulnerableAliases(string $string) : string
    {
        $string = str_ireplace(
            array_keys($this->vulnerable_aliases),
            array_values($this->vulnerable_aliases),
            $string
        );

        return $string;
    }

    /**
     * @param string $string
     * @return bool
     */
    protected function checkStringIsCyrillic(string $string) : bool
    {
        if (preg_match('#[А-Яа-я]#i', $string) === 1) {
            return true;
        }

        return false;
    }

    /**
     * @param string $string
     * @return string
     */
    protected function clearStringFromSymbols(string $string) : string
    {
        $string = preg_replace(
            [
                '#[^\w\s]|\d#i',
                '#\b[^\s]{1,3}\b#i',
                '#\s{2,}#i',
                '#^\s+|\s+$#i'
            ],
            [
                '',
                '',
                ' '
            ],
            $string
        );

        $string = str_replace(' ', '', $string);
        $string = str_replace('-', '', $string);

        return $string;
    }

    /**
     * @param string $string
     * @return string|null
     */
    public function getSound(string $string)
    {
        $is_cyrillic = false;

        if ($this->checkStringIsCyrillic($string)) {
            $string = $this->replaceVulnerableAliases($string);
            
            $string = $this->transliterate($string);
            $is_cyrillic = true;
        }

        $string = strtoupper($string);
        $string = $this->clearStringFromSymbols($string);
        $string = substr($string, 0, 8);

        if (!isset($string[0])) {
            return null;
        }

        $matches = explode(' ', $string);

        foreach($matches as $key => $match) {
            $matches[$key] = $this->getWordSound($match, $is_cyrillic);
        }

        return implode('.', $matches);
    }

    /**
     * @param $string
     * @return mixed
     */
    protected function transliterate(string $string) : string
    {
        static $ru = [
            'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е', 'Ё', 'ё', 'Ж', 'ж', 'З', 'з',
            'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',
            'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ',
            'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь', 'Э', 'э', 'Ю', 'ю', 'Я', 'я'
        ];

        static $en = [
            'A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e', 'E', 'e', 'Zh', 'zh', 'Z', 'z',
            'I', 'i', 'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r',
            'S', 's', 'T', 't', 'U', 'u', 'F', 'f', 'H', 'h', 'C', 'c', 'Ch', 'ch', 'Sh', 'sh', 'Sch', 'sch',
            '\'', '\'', 'Y', 'y',  '\'', '\'', 'E', 'e', 'Ju', 'ju', 'Ja', 'ja'
        ];

        $string = str_replace($ru, $en, $string);

        return $string;
    }
}
