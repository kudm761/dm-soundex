<?php
return [
    'A' => [
        0 => [0, -1, -1],
        'I' => [
            [0, 1, -1]
        ],
        'J' => [
            [0, 1, -1]
        ],
        'Y' => [
            [0, 1, -1]
        ],
        'U' => [
            [0, 7, -1]
        ]
    ],
    'B' => [
        [7, 7, 7]
    ],
    'C' => [
        0 => [5, 5, 5],
        1 => [4, 4, 4],
        'Z' => [
            0 => [4, 4, 4],
            'S' => [
                [4, 4, 4]
            ]
        ],
        'S' => [
            0 => [4, 4, 4],
            'Z' => [
                [4, 4, 4]
            ]
        ],
        'K' => [
            [5, 5, 5],
            [45, 45, 45]
        ],
        'H' => [
            0 => [5, 5, 5],
            1 => [4, 4, 4],
            'S' => [
                [5, 54, 54]
            ]
        ]
    ],
    'D' => [
        0 => [3, 3, 3],
        'T' => [
            [3, 3, 3]
        ],
        'Z' => [
            0 => [4, 4, 4],
            'H' => [
                [4, 4, 4]
            ],
            'S' => [
                [4, 4, 4]
            ]
        ],
        'S' => [
            0 => [4, 4, 4],
            'H' => [
                [4, 4, 4]
            ],
            'Z' => [
                [4, 4, 4]
            ]
        ],
        'R' => [
            'S' => [
                [4, 4, 4]
            ],
            'Z' => [
                [4, 4, 4]
            ]
        ]
    ],
    'E' => [
        0 => [0, -1, -1],
        'I' => [
            [0, 1, -1]
        ],
        'J' => [
            [0, 1, -1]
        ],
        'Y' => [
            [0, 1, -1]
        ],
        'U' => [
            [1, 1, -1]
        ],
        'W' => [
            [1, 1, -1]
        ]
    ],
    'F' => [
        0 => [7, 7, 7],
        'B' => [
            [7, 7, 7]
        ]
    ],
    'G' => [
        [5, 5, 5]
    ],
    'H' => [
        [5, 5, -1]
    ],
    'I' => [
        0 => [0, -1, -1],
        'A' => [
            [1, -1, -1]
        ],
        'E' => [
            [1, -1, -1]
        ],
        'O' => [
            [1, -1, -1]
        ],
        'U' => [
            [1, -1, -1]
        ]
    ],
    'J' => [
        [4, 4, 4]
    ],
    'K' => [
        0 => [5, 5, 5],
        'H' => [
            [5, 5, 5]
        ],
        'S' => [
            [5, 54, 54]
        ]
    ],
    'L' => [
        [8, 8, 8]
    ],
    'M' => [
        0 => [6, 6, 6],
        'N' => [
            [66, 66, 66]
        ]
    ],
    'N' => [
        0 => [6, 6, 6],
        'M' => [
            [66, 66, 66]
        ]
    ],
    'O' => [
        0 => [0, -1, -1],
        'I' => [
            [0, 1, -1]
        ],
        'J' => [
            [0, 1, -1]
        ],
        'Y' => [
            [0, 1, -1]
        ]
    ],
    'P' => [
        0 => [7, 7, 7],
        'F' => [
            [7, 7, 7]
        ],
        'H' => [
            [7, 7, 7]
        ]
    ],
    'Q' => [
        [5, 5, 5]
    ],
    'R' => [
        0 => [9, 9, 9],
        'Z' => [
            [94, 94, 94],
            [94, 94, 94]
        ],
        'S' => [
            [94, 94, 94],
            [94, 94, 94]
        ]
    ],
    'S' => [
        0 => [4, 4, 4],
        'Z' => [
            0 => [4, 4, 4],
            'T' => [
                [2, 43, 43]
            ],
            'C' => [
                'Z' => [
                    [2, 4, 4]
                ],
                'S' => [
                    [2, 4, 4]
                ]
            ],
            'D' => [
                [2, 43, 43]
            ]
        ],
        'D' => [
            [2, 43, 43]
        ],
        'T' => [
            0 => [2, 43, 43],
            'R' => [
                'Z' => [
                    [2, 4, 4]
                ],
                'S' => [
                    [2, 4, 4]
                ]
            ],
            'C' => [
                'H' => [
                    [2, 4, 4]
                ]
            ],
            'S' => [
                'H' => [
                    [2, 4, 4]
                ],
                'C' => [
                    'H' => [
                        [2, 4, 4]
                    ]
                ]
            ]
        ],
        'C' => [
            0 => [2, 4, 4],
            'H' => [
                0 => [4, 4, 4],
                'T' => [
                    0 => [2, 43, 43],
                    'S' => [
                        'C' => [
                            'H' => [
                                [2, 4, 4]
                            ]
                        ],
                        'H' => [
                            [2, 4, 4]
                        ]
                    ],
                    'C' => [
                        'H' => [
                            [2, 4, 4]
                        ]
                    ]
                ],
                'D' => [
                    [2, 43, 43]
                ]
            ]
        ],
        'H' => [
            0 => [4, 4, 4],
            'T' => [
                0 => [2, 43, 43],
                'C' => [
                    'H' => [
                        [2, 4, 4]
                    ]
                ],
                'S' => [
                    'H' => [
                        [2, 4, 4]
                    ]
                ]
            ],
            'C' => [
                'H' => [
                    [2, 4, 4]
                ]
            ],
            'D' => [
                [2, 43, 43]
            ]
        ]
    ],
    'T' => [
        0 => [3, 3, 3],
        'C' => [
            0 => [4, 4, 4],
            'H' => [
                [4, 4, 4]
            ]
        ],
        'Z' => [
            0 => [4, 4, 4],
            'S' => [
                [4, 4, 4]
            ]
        ],
        'S' => [
            0 => [4, 4, 4],
            'Z' => [
                [4, 4, 4]
            ],
            'H' => [
                [4, 4, 4]
            ],
            'C' => [
                'H' => [
                    [4, 4, 4]
                ]
            ]
        ],
        'T' => [
            'S' => [
                0 => [4, 4, 4],
                'Z' => [
                    [4, 4, 4]
                ],
                'C' => [
                    'H' => [
                        [4, 4, 4]
                    ]
                ]
            ],
            'C' => [
                'H' => [
                    [4, 4, 4]
                ]
            ],
            'Z' => [
                [4, 4, 4]
            ]
        ],
        'H' => [
            [3, 3, 3]
        ],
        'R' => [
            'Z' => [
                [4, 4, 4]
            ],
            'S' => [
                [4, 4, 4]
            ]
        ]
    ],
    'U' => [
        0 => [0, -1, -1],
        'E' => [
            [0, -1, -1]
        ],
        'I' => [
            [0, 1, -1]
        ],
        'J' => [
            [0, 1, -1]
        ],
        'Y' => [
            [0, 1, -1]
        ]
    ],
    'V' => [
        [7, 7, 7]
    ],
    'W' => [
        [7, 7, 7]
    ],
    'X' => [
        [5, 54, 54]
    ],
    'Y' => [
        [1, -1, -1]
    ],
    'Z' => [
        0 => [4, 4, 4],
        'D' => [
            0 => [2, 43, 43],
            'Z' => [
                0 => [2, 4, 4],
                'H' => [
                    [2, 4, 4]
                ]
            ]
        ],
        'H' => [
            0 => [4, 4, 4],
            'D' => [
                0 => [2, 43, 43],
                'Z' => [
                    'H' => [
                        [2, 4, 4]
                    ]
                ]
            ]
        ],
        'S' => [
            0 => [4, 4, 4],
            'H' => [
                [4, 4, 4]
            ],
            'C' => [
                'H' => [
                    [4, 4, 4]
                ]
            ]
        ]
    ]
];