<?php
class DmSoundexTest extends \PHPUnit\Framework\TestCase
{
    protected $soundex = null;

    public function setUp()
    {
        $this->soundex = new \DmSoundex\DmSoundex();
    }

    public function tearDown()
    {
        unset($this->soundex);
    }

    public function testDmSoundexInstantiated()
    {
        $soundex = new \DmSoundex\DmSoundex();

        $this->assertInstanceOf('\DmSoundex\DmSoundex', $soundex);
    }

    public function testAllCodesExists()
    {
        $reflection = new \ReflectionClass($this->soundex);
        $codes_property = $reflection->getProperty('codes');
        $codes_property->setAccessible(true);

        $codes = $codes_property->getValue($this->soundex);

        $this->assertTrue(is_array($codes), 'Codes should be an array');
        $this->assertNotEmpty($codes, 'Codes should be not empty');

        foreach (range('A', 'Z') as $key) {
            $this->assertArrayHasKey($key, $codes, sprintf('Codes has no values for: `%s`', $key));
        }
    }

    public function testSoundsAreEqual()
    {
        $test_pairs = [
            ['Jonathan Ive', 'Jonatan Ivy'],
            ['Санкт-Петербург', 'Санкт-питирбург'],
            ['Волгоград', 'Волгаград'],
            ['Москва', 'Масква'],
            ['Один два три', 'адин двА тРы']
        ];

        foreach ($test_pairs as $test_pair) {
            $sound1 = $this->soundex->getSound($test_pair[0]);
            $sound2 = $this->soundex->getSound($test_pair[1]);

            $this->assertEquals($sound1, $sound2, sprintf('Sounds are not equal for pair: %s', implode(':', $test_pair)));
        }
    }

    public function testSoundsAreNotEqual()
    {
        $test_pairs = [
            ['Санкт-Петербург', 'Волгаград'],
            ['Волгоград', 'Санкт-питирбург'],
            ['Москва', 'река'],
            ['Один два три', '1 2 3']
        ];

        foreach ($test_pairs as $test_pair) {
            $sound1 = $this->soundex->getSound($test_pair[0]);
            $sound2 = $this->soundex->getSound($test_pair[1]);

            $this->assertNotEquals($sound1, $sound2, sprintf('Sounds should not be equal for pair: %s', implode(':', $test_pair)));
        }
    }
}